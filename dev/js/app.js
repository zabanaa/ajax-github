(function(){

	var body = document.body; // store the body

	var theRequest = new XMLHttpRequest();

	// Cache Dom Elements
	var userForm = {

		input: document.querySelector('.username__input'),
		label: document.querySelector('.username__label'),
		submit: document.querySelector('.username__submit'),
		card: document.querySelector('.user-profile')

	}

	// Fetch user info (this is the function that will make the ajax call)
	function getUserInfo() {

		// store the request URL
		var requestURL = "https://api.github.com/users/" + userForm.input.value;

		// Clear the input
		clearInput();

		// check the readyState change
		theRequest.onreadystatechange = function() {
			// if readyState = 4 && status = 200 ie if the request is successful
			if ( theRequest.readyState === 4 ) {

				if ( theRequest.status === 200 ) {

					// Display the user's profile
					displayUserProfile(theRequest);

				} else {

					// Display an error
					displayError("There was an error " + theRequest.status + ", Please enter a valid username");

				}

			}

		}

		// Open the request
		theRequest.open('GET', requestURL, true);

		// Send it
		theRequest.send();

		// change the state of the app aka fade out the form and show the card
		changeState('user-profile-state');

	}

	// Function to display the users Info if the get request is successful
	function displayUserProfile(ajaxRequest) {

		// This function takes the request as a parameter
		// Then parse the JSON responseText returned by the request

		var data = JSON.parse(ajaxRequest.responseText);


		// Use Mustache to display the information dynamically in the markup
		var template = document.getElementById('userInfo').innerHTML; // reference the template
		var jsonHTML = Mustache.render(template, data); // compile it
		var output = userForm.card; // reference the output section

		output.innerHTML = jsonHTML; // insert the compiled template inside the section

	}

	// Function to display an error, if the GET request fails
	function displayError(errMsg) {
		alert(errMsg);
	}

	// Function to change the state of the app
	function changeState(newBodyClass) {

		body.setAttribute('class', newBodyClass); // Add the new Body class

		// If the app is showing the profile card
		if ( newBodyClass === 'user-profile-state' ) {

			// When the user clicks on the bio button, show the bio (Event Delegation)
			userForm.card.addEventListener('click', showBio, false);

		} else if (newBodyClass === 'username-state') {
			// WHen you exit the card and go back to the form, remove the event handler
			userForm.card.removeEventListener('click', showBio);
		}


	}

	function showBio(event) {

		//If the element clicked is a the showBio button

		if (event.target && event.target.className == 'user-profile__show-bio') {

			var bioSection = event.target.nextElementSibling; // store the next sibling (aka the hidden bio)
			var bio = bioSection.querySelector('p'); // select the paragraph inside the blockquote
			bioSection.classList.toggle("showing"); // toggle the class of showing to hide/show the bio

			if	(bio.innerHTML == '') {
				// If the user has not provided a bio
				bio.innerHTML = "There is no bio for this user, sorry"; // show this message instead of an empty div

			}

		} else if (event.target && (event.target.className == 'backBtn') || (event.target.className == 'octicon octicon-x') ) {

			// If the element clicked is the backBtn
			changeState('username-state'); // change the state of the app to username (ie show the form and hide the card)

		}


	}

	// Show the input when the user clicks on the input label (the pink rectangle)
	function showUserNameInput() {

		this.classList.add('clicked'); // Add class of clicked to label
		this.nextElementSibling.classList.add('filled'); // Add class of filled to input

	}

	// When the user clicks away from the form ...
	function hideUserNameInput() {

		if ( (userForm.input.classList.contains('filled')) && (userForm.label.classList.contains('clicked')) ) {

			// If the input is shown and the label is hidden

			userForm.input.classList.remove('filled'); // Hide the input
			userForm.label.classList.remove('clicked'); // Show the label
			userForm.submit.classList.remove('showing'); // Hide the submit button

		}

	}

	// When the input is focused, show the submit button
	function showButton(event) {

		if (userForm.input.value.length >= 0 || userForm.input.value ) {
			userForm.submit.disabled = false; // Enable the button
			userForm.submit.classList.add('showing');
		}

		if (userForm.input.value === "" && event.keyCode == 13) {
			event.preventDefault();
		}

	}

	// Clear the input
	function clearInput() {
		userForm.input.value = "";
	}

	// Event Listeners
	userForm.label.addEventListener('click', showUserNameInput, false);
	userForm.input.addEventListener('blur', hideUserNameInput, false);
	userForm.input.addEventListener('keypress', showButton, false);
	userForm.input.addEventListener('keypress', function(event) {
		if (event.keyCode == 13 && userForm.input.value != "") {
			getUserInfo();
			event.preventDefault();
		}
	});
	userForm.input.addEventListener('focus', showButton, false);
	userForm.submit.addEventListener('click', getUserInfo,false);



}());
